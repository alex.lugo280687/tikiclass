﻿namespace Login 
{
    using Xamarin.Forms;
    //Usamos views para usar la carpeta creada.
    using Views;

    public partial class App : Application
	{
        #region Constructor
        public App()
        {
            InitializeComponent();
            
            //Cambiamos a la plantilla que tenemos para iniciarla. 
            MainPage = new NavigationPage(new LoginPage());
        }
        #endregion

        #region Methods
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        } 
        #endregion
    }
}
